package br.com.kiosfera.dribbbleclient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

@SuppressLint("NewApi")
public class ActVerImagemActivity extends Activity {
	public static final String EXTRA_URL_IMAGEM = "URLIMAGEM";
	private ImageView imgShot;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_ver_imagem);

		imgShot = (ImageView) findViewById(R.id.imgShot);
		if (!getIntent().hasExtra(EXTRA_URL_IMAGEM)) {
			finish();
			return;
		}

		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Picasso.with(this).load(getIntent().getExtras().getString(EXTRA_URL_IMAGEM)).into(imgShot);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

}
