package br.com.kiosfera.dribbbleclient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.kiosfera.dribbbleclient.model.Shot;
import br.com.kiosfera.dribbbleclient.task.BaseTask;
import br.com.kiosfera.dribbbleclient.task.BaseTask.TIPO_ACAO;
import br.com.nrtech.utils.ErrorUtils;

import com.squareup.picasso.Picasso;

@SuppressLint("NewApi")
public class ActDetShotActivity extends Activity {

	public static final String EXTRA_ID_SHOT = "IDSHOT";

	private CircleImageView imgFotoUsuario;
	private ImageView imgShot;
	private TextView txtNomePessoa;
	private TextView txtQtdeViews;
	private TextView txtNomeShot;
	private TextView txtDescricao;
	private Shot sh;

	private int idShot;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_det_shot);

		imgFotoUsuario = (CircleImageView) findViewById(R.id.imgFotoUsuario);
		imgShot = (ImageView) findViewById(R.id.imgShot);
		txtNomePessoa = (TextView) findViewById(R.id.txtNomePessoa);
		txtQtdeViews = (TextView) findViewById(R.id.txtQtdeViews);
		txtNomeShot = (TextView) findViewById(R.id.txtNomeShot);
		txtDescricao = (TextView) findViewById(R.id.txtDescricao);
		txtDescricao.setMovementMethod(LinkMovementMethod.getInstance());

		if (!getIntent().hasExtra(EXTRA_ID_SHOT)) {
			finish();
			return;
		}

		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		imgShot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in = new Intent(ActDetShotActivity.this, ActVerImagemActivity.class);
				in.putExtra(ActVerImagemActivity.EXTRA_URL_IMAGEM, sh.getImage_url());
				startActivity(in);
			}
		});

		idShot = getIntent().getExtras().getInt(EXTRA_ID_SHOT);
		new BaseTask(this, true, handlerShot, TIPO_ACAO.Detalhe).execute(idShot);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private Handler handlerShot = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what != BaseTask.WHAT_OK)
				return;

			try {
				if (!(msg.obj instanceof Shot)) {
					finish();
					return;
				}

				sh = (Shot) msg.obj;
				Picasso.with(ActDetShotActivity.this).load(sh.getImage_url()).into(imgShot);
				Picasso.with(ActDetShotActivity.this).load(sh.getPlayer().getAvatar_url()).into(imgFotoUsuario);
				txtNomePessoa.setText(sh.getPlayer().getName());
				txtDescricao.setText(sh.getDescription() == null ? "" : Html.fromHtml(sh.getDescription()));
				txtNomeShot.setText(sh.getTitle());
				txtQtdeViews.setText(String.valueOf(sh.getViews_count()));

				setTitle(sh.getTitle());

			} catch (Exception ex) {
				ErrorUtils.trataErros(ex, null, true, ActDetShotActivity.this);
			}

			super.handleMessage(msg);
		}
	};

}
