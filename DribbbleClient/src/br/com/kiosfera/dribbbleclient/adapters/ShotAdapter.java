package br.com.kiosfera.dribbbleclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.kiosfera.dribbbleclient.R;
import br.com.kiosfera.dribbbleclient.model.Result;
import br.com.kiosfera.dribbbleclient.model.Shot;

import com.squareup.picasso.Picasso;

public class ShotAdapter extends BaseAdapter {

	private Result objConsulta;
	private Context context;

	public ShotAdapter(Context context, Result objConsulta) {
		this.context = context;
		this.objConsulta = objConsulta;
	}

	@Override
	public int getCount() {
		return objConsulta.getShots() != null ? objConsulta.getShots().size() : 0;
	}

	@Override
	public Shot getItem(int position) {
		return objConsulta.getShots() != null ? objConsulta.getShots().get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imgShot;
		TextView txtNome;
		TextView txtViews;

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.det_list_shot, parent, false);
			imgShot = (ImageView) convertView.findViewById(R.id.imgShot);
			txtNome = (TextView) convertView.findViewById(R.id.txtNomeShot);
			txtViews = (TextView) convertView.findViewById(R.id.txtQtdeViews);
			convertView.setTag(new ViewHolder(imgShot, txtNome, txtViews));
		} else {
			ViewHolder vHolder = (ViewHolder) convertView.getTag();
			imgShot = vHolder.imgShot;
			txtNome = vHolder.txtNome;
			txtViews = vHolder.txtViews;
		}

		Shot sh = getItem(position);
		Picasso.with(context).load(sh.getImage_url()).into(imgShot);
		txtNome.setText(sh.getTitle());
		txtViews.setText(String.valueOf(sh.getViews_count()));

		return convertView;
	}

	private static class ViewHolder {
		public final ImageView imgShot;
		public final TextView txtNome;
		public final TextView txtViews;

		public ViewHolder(ImageView imgShot, TextView txtNome, TextView txtViews) {
			this.imgShot = imgShot;
			this.txtNome = txtNome;
			this.txtViews = txtViews;
		}
	}

}
