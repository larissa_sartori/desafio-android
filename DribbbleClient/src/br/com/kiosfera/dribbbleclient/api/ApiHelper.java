package br.com.kiosfera.dribbbleclient.api;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedByteArray;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ApiHelper {
	public static String END_POINT_SERVICE = "http://api.dribbble.com";

	public static DribbbleService getService() {
		return buildService();
	}

	private static DribbbleService buildService() {
		Gson gson = new GsonBuilder().create();
		RestAdapter restAdp = new RestAdapter.Builder().setConverter(new GsonConverter(gson)).setEndpoint(END_POINT_SERVICE)
				.setErrorHandler(new ErrorHandler() {
					@Override
					public Throwable handleError(RetrofitError error) {
						String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
						return new Exception(json);
					}
				}).build();
		return restAdp.create(DribbbleService.class);
	}
}
