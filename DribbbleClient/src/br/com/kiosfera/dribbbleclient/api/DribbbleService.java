package br.com.kiosfera.dribbbleclient.api;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import br.com.kiosfera.dribbbleclient.model.Result;
import br.com.kiosfera.dribbbleclient.model.Shot;

public interface DribbbleService {
	@GET("/shots/popular")
	Result getShots(@Query("page") int page);

	@GET("/shots/{token}")
	Shot getDetailShot(@Path("token") int token);
}
