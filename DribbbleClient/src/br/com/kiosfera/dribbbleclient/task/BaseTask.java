package br.com.kiosfera.dribbbleclient.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import br.com.kiosfera.dribbbleclient.R;
import br.com.kiosfera.dribbbleclient.api.ApiHelper;
import br.com.kiosfera.dribbbleclient.model.Result;
import br.com.kiosfera.dribbbleclient.model.Shot;
import br.com.nrtech.utils.ErrorUtils;
import br.com.nrtech.utils.Mensagens;

public class BaseTask extends AsyncTask<Object, Void, Object> {

	public static int WHAT_OK = 101;
	public static int WHAT_ERROR = 102;

	public enum TIPO_ACAO {
		Listar, Detalhe
	}

	protected Context context;
	protected boolean showWait;
	protected ProgressDialog pdWait;
	protected Handler handler;
	protected TIPO_ACAO tipoAcao;

	public BaseTask(Context context, boolean showWait, Handler handler, TIPO_ACAO tipoAcao) {
		this.context = context;
		this.showWait = showWait;
		this.handler = handler;
		this.tipoAcao = tipoAcao;
	}

	@Override
	protected void onPreExecute() {
		if (showWait)
			this.pdWait = ProgressDialog.show(this.context, this.context.getString(R.string.app_name), "Aguarde...");

		super.onPreExecute();
	}

	@Override
	protected Object doInBackground(Object... params) {
		try {
			switch (this.tipoAcao) {
			case Listar:
				int numeroPagina = (int) params[0];
				Result lista = ApiHelper.getService().getShots(numeroPagina);
				return lista;
			case Detalhe:
				int idShot = (int) params[0];
				Shot retorno = ApiHelper.getService().getDetailShot(idShot);
				return retorno;
			default:
				break;
			}
		} catch (Exception ex) {
			ErrorUtils.trataErros(ex, null, false, context);
			System.out.println("Erro recuperar FAQ: " + ex.getMessage());
			return ex;
		}

		return null;
	}

	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);

		if (result == null || (result instanceof Exception)) {
			Mensagens.showMessage(context.getString(R.string.app_name), ((Exception) result).getMessage(), null, context).show();
			handler = null;
		}

		if (handler != null) {
			Message msg = new Message();
			msg.what = WHAT_OK;
			msg.obj = result;
			handler.sendMessage(msg);
		}

		if (pdWait != null && pdWait.isShowing())
			pdWait.dismiss();
	}
}
