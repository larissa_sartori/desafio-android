package br.com.kiosfera.dribbbleclient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import br.com.kiosfera.dribbbleclient.adapters.ShotAdapter;
import br.com.kiosfera.dribbbleclient.model.Result;
import br.com.kiosfera.dribbbleclient.task.BaseTask;
import br.com.kiosfera.dribbbleclient.task.BaseTask.TIPO_ACAO;
import br.com.nrtech.utils.ButtonMsg;
import br.com.nrtech.utils.ErrorUtils;
import br.com.nrtech.utils.Mensagens;

@SuppressLint("NewApi")
public class ActPrincipal extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks, OnRefreshListener {

	private ShotAdapter adpShot;
	private ListView lvShots;
	private Result objResult;
	private ImageView imgVoltar;
	private ImageView imgProxima;
	private TextView txtPaginacao;
	private SwipeRefreshLayout swipeRefresh;

	private NavigationDrawerFragment mNavigationDrawerFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_principal);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);

		lvShots = (ListView) findViewById(R.id.lvShots);
		imgProxima = (ImageView) findViewById(R.id.imgProxima);
		imgVoltar = (ImageView) findViewById(R.id.imgVoltar);
		txtPaginacao = (TextView) findViewById(R.id.txtIndicaPagina);
		swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
		swipeRefresh.setOnRefreshListener(this);

		imgProxima.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				carregaResultados(objResult.getPage() + 1);
			}
		});

		imgVoltar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				carregaResultados(objResult.getPage() - 1);
			}
		});

		txtPaginacao.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				solicitaPagina();
			}
		});

		lvShots.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent in = new Intent(ActPrincipal.this, ActDetShotActivity.class);
				in.putExtra(ActDetShotActivity.EXTRA_ID_SHOT, adpShot.getItem(position).getId());
				startActivity(in);
			}
		});

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
		carregaResultados(1);
	}

	private void carregaResultados(int pagina) {
		if (pagina == 0)
			pagina = 1;

		new BaseTask(this, true, handlerShots, TIPO_ACAO.Listar).execute(pagina);
	}

	private Handler handlerShots = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what != BaseTask.WHAT_OK)
				return;

			try {
				if (!(msg.obj instanceof Result))
					return;

				objResult = (Result) msg.obj;
				adpShot = new ShotAdapter(ActPrincipal.this, objResult);
				lvShots.setAdapter(adpShot);

				imgVoltar.setVisibility(objResult.getPage() > 1 ? View.VISIBLE : View.INVISIBLE);
				imgProxima.setVisibility(objResult.getPage() == objResult.getPages() ? View.INVISIBLE : View.VISIBLE);
				txtPaginacao.setText(String.format("%d de %d", objResult.getPage(), objResult.getPages()));

			} catch (Exception ex) {
				ErrorUtils.trataErros(ex, null, true, ActPrincipal.this);
			}

			swipeRefresh.setRefreshing(false);
			super.handleMessage(msg);
		}
	};

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		switch (position) {
		case 0:
			carregaResultados(1);
			break;
		case 1:
			carregaResultados(objResult.getPages());
			break;
		case 2:
			solicitaPagina();
			break;
		default:
			break;
		}
	}

	public void solicitaPagina() {
		final EditText edtPagina = new EditText(ActPrincipal.this);
		edtPagina.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
		edtPagina.setText(String.valueOf(objResult.getPage()));
		edtPagina.selectAll();

		ButtonMsg botaoOK = new ButtonMsg("Ir", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (edtPagina.getText().toString().isEmpty())
					return;

				int novaPagina = Integer.valueOf(edtPagina.getText().toString());
				if (novaPagina > objResult.getPages())
					novaPagina = objResult.getPages();
				if (novaPagina < 1)
					novaPagina = 1;

				carregaResultados(novaPagina);
			}
		});

		Mensagens.inputBox("Ir para página", "Número da página", edtPagina, botaoOK, ActPrincipal.this).show();
	}

	@Override
	public void onRefresh() {
		swipeRefresh.setRefreshing(true);
		carregaResultados(objResult.getPage());
	}
}
