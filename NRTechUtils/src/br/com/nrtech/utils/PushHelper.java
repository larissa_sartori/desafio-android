package br.com.nrtech.utils;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;


public class PushHelper {

	public interface OnRegistrationListener {
		public abstract void onEndRegistration(String regID);

		public abstract void onError(String msg);
	}

	public static interface CheckPlayService {
		public static final int OK = 1;
		public static final int NAO_SUPORTADO = 2;
		public static final int EXIBE_MENSAGEM = 3;
	}

	public static interface PushConfig {
		public static final String PROPERTY_REG_ID = "registration_id";
		public static final String PROPERTY_APP_VERSION = "appVersion";
	}

	private String senderID;
	private int playServicesResolutionRequest = 9000;
	private Context context;
	private GoogleCloudMessaging gcm;
	private String TAG_LOG = "PushHelper";
	private String registrationId;
	private OnRegistrationListener regListener;

	public PushHelper(Context context, String senderID, OnRegistrationListener regListener) {
		this.context = context;
		this.senderID = senderID;
		this.regListener = regListener;
	}

	public int checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, playServicesResolutionRequest).show();
				return CheckPlayService.EXIBE_MENSAGEM;
			} else
				Log.i(TAG_LOG, "This device is not supported.");

			return CheckPlayService.NAO_SUPORTADO;
		}
		return CheckPlayService.OK;
	}

	public String getRegistrationId() {
		final SharedPreferences prefs = getGCMPreferences();
		registrationId = prefs.getString(PushConfig.PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG_LOG, "Registration not found.");
			return "";
		}

		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PushConfig.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = General.getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG_LOG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	public SharedPreferences getGCMPreferences() {
		return context.getSharedPreferences("push", Context.MODE_PRIVATE);
	}

	public void registerToPush() throws Exception {
		switch (checkPlayServices()) {
		case CheckPlayService.NAO_SUPORTADO:
			throw new Exception("Serviços de Push não suportados para este aparelho");
		case CheckPlayService.EXIBE_MENSAGEM:
			return;
		default:
			break;
		}

		this.gcm = GoogleCloudMessaging.getInstance(context);
		getRegistrationId();

		if (registrationId.isEmpty()) {
			registerInBackground();
		}
	}

	@SuppressWarnings("unchecked")
	public void registerInBackground() {
		new AsyncTask() {

			@Override
			protected void onPostExecute(Object msg) {
				if (PushHelper.this.regListener != null)
					PushHelper.this.regListener.onEndRegistration(registrationId);
			}

			@Override
			protected Object doInBackground(Object... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					String regid = gcm.register(senderID);
					msg = "Device registered, registration ID=" + regid;
					storeRegistrationId();
				} catch (IOException ex) {
					ErrorUtils.trataErros(ex, null, false, context);
					if (PushHelper.this.regListener != null)
						PushHelper.this.regListener.onError("Error :" + ex.getMessage());
				}
				return msg;
			}
		}.execute(null, null, null);
	}

	public void storeRegistrationId() {
		final SharedPreferences prefs = getGCMPreferences();
		int appVersion = General.getAppVersion(context);
		Log.i(TAG_LOG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PushConfig.PROPERTY_REG_ID, registrationId);
		editor.putInt(PushConfig.PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	public String getSenderID() {
		return senderID;
	}

	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

	public int getPlayServicesResolutionRequest() {
		return playServicesResolutionRequest;
	}

	public void setPlayServicesResolutionRequest(int playServicesResolutionRequest) {
		this.playServicesResolutionRequest = playServicesResolutionRequest;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public String getTAG_LOG() {
		return TAG_LOG;
	}

	public void setTAG_LOG(String tAG_LOG) {
		TAG_LOG = tAG_LOG;
	}

	public OnRegistrationListener getRegListener() {
		return regListener;
	}

	public void setRegListener(OnRegistrationListener regListener) {
		this.regListener = regListener;
	}

}
