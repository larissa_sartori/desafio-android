package br.com.nrtech.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TimePicker;
import android.widget.Toast;



public class Mensagens {

	private static int ret_dia = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	private static int ret_mes = Calendar.getInstance().get(Calendar.MONTH);
	private static int ret_ano = Calendar.getInstance().get(Calendar.YEAR);

	private static int ret_hora = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
	private static int ret_minuto = Calendar.getInstance().get(Calendar.MINUTE);

	private static Method metodoAfterSetDate;
	private static Method metodoAfterSetTime;
	private static Object objAfterSetDateTime;

	public static ArrayList<Dialog> openedMessages = new ArrayList<Dialog>();

	/**
	 * Função responsável por fechar todos os Dialogs (mensagens) que estão
	 * abertos e limpar/apagar a lista de mensagens através da função
	 * clearHideMessages()
	 * 
	 */
	public static void closeAllOpenMessages() {
		try {
			for (Dialog dialog : openedMessages) {
				if (dialog != null || (dialog != null && dialog.isShowing())) {
					dialog.dismiss();
				}
			}
			clearHideMessages();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Função responsável por limpar/apagar todas as mensagens armazenadas no
	 * array openedMessages (variável global do tipo ArrayList<Dialog>)
	 */
	public static void clearHideMessages() {
		try {
			openedMessages.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static TimePickerDialog.OnTimeSetListener mSetTimeDialog = new OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hour, int minute) {
			ret_hora = hour;
			ret_minuto = minute;

			try {
				metodoAfterSetTime.invoke(objAfterSetDateTime, new Object[] { ret_hora, ret_minuto });
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	};

	private static DatePickerDialog.OnDateSetListener mSetDateDialog = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			ret_dia = dayOfMonth;
			ret_mes = monthOfYear + 1;
			ret_ano = year;

			try {
				metodoAfterSetDate.invoke(objAfterSetDateTime, new Object[] { ret_dia, ret_mes, ret_ano });
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	};

	/**
	 * Cria uma caixa de diálogo para seleção de data Exemplo de utilização:
	 * <code>
	 * 		private Method med = null;
	 * 
	 * 		public class FLancarAlterarPedidoCotacaoProposta extends TabActivityBasico {
	 * 			{...}
	 * 			med = FLancarAlterarPedidoCotacaoProposta.class.getMethod("recebeData", new Class[]{int.class,int.class,int.class});
	 * 		}
	 * 
	 * 		public void recebeData(int dia, int mes, int ano) {
	 *  		int teste = dia+mes+ano;
	 * 			System.out.println(teste);
	 * 		}
	 * 
	 * 		((Button)tbHost.getCurrentView().findViewById(R.id.botaoData01)).setOnClickListener(new OnClickListener() {
						
				public void onClick(View arg0) {
						FLancarAlterarPedidoCotacaoProposta teste = new FLancarAlterarPedidoCotacaoProposta();
						Mensagens.solicitaData(Convert.DateToStr(new Date()), med, teste);
					}
				});
	 * </code>
	 * 
	 * @param dataInicial
	 *            A data a ser exibida inicialmente no diálogo. Quando for null,
	 *            seleciona a data corrente.
	 * @param retorno
	 *            Método a ser chamado após o "OK" no diálogo. O método deve
	 *            conter 3 parâmetros do tipo "int" (dia,mes,ano)
	 * @param objeto
	 *            Instancia da classe de origem que possui o método
	 * @throws Exception
	 */

	public static void solicitaData(String dataInicial, Method retorno, Object objeto, Context context) throws Exception {
		try {
			if (retorno == null || objeto == null) {
				throw new Exception("solicitaData: O método de retorno e o objeto não podem ser nulos.");
			}

			if (dataInicial != null) {
				ret_ano = Integer.parseInt(dataInicial.substring(6));
				ret_mes = Integer.parseInt(dataInicial.substring(3, 5)) - 1;
				ret_dia = Integer.parseInt(dataInicial.substring(0, 2));
			} else {
				ret_dia = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
				ret_mes = Calendar.getInstance().get(Calendar.MONTH);
				ret_ano = Calendar.getInstance().get(Calendar.YEAR);
			}

			metodoAfterSetDate = retorno;
			objAfterSetDateTime = objeto;

			Dialog setData = new DatePickerDialog(context, mSetDateDialog, ret_ano, ret_mes, ret_dia);
			setData.show();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw e;
		}
	}

	public static void solicitaHora(String horaInicial, Method retorno, Object objeto, Context context) throws Exception {
		try {
			if (retorno == null || objeto == null) {
				throw new Exception("solicitaHora: O método de retorno e o objeto não podem ser nulos.");
			}

			if (horaInicial != null) {
				ret_hora = Integer.parseInt(horaInicial.substring(0, 2));
				ret_minuto = Integer.parseInt(horaInicial.substring(3));
			} else {
				ret_hora = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
				ret_minuto = Calendar.getInstance().get(Calendar.MINUTE);
			}

			metodoAfterSetTime = retorno;
			objAfterSetDateTime = objeto;

			Dialog setTime = new TimePickerDialog(context, mSetTimeDialog, ret_hora, ret_minuto, true);
			setTime.show();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Cria uma caixa de mensagem simples, apenas com textos e botões
	 * 
	 * @param context
	 * @param titulo
	 * @param mensagem
	 * @param buttonsMsg
	 * @return Builder
	 */
	private static AlertDialog.Builder criarDialogo(Context context, String titulo, String mensagem, ButtonMsg[] buttonsMsg, View conteudo, boolean isCustom, Boolean showKey, int iconId) {
		try {
			final Builder retorno = new Builder(context);
			retorno.setMessage(mensagem);

			if (titulo != null)
				retorno.setTitle(titulo);

			includeKeyListener(retorno);

			retorno.setCancelable(isCustom);
			if (iconId != 0)
				retorno.setIcon(iconId);

			if (conteudo != null) {
				retorno.setView(conteudo);
			}

			if (buttonsMsg == null || buttonsMsg.length == 0) {
				ButtonMsg btOK = new ButtonMsg();
				btOK.setTexto("OK");
				btOK.setTipo(DialogInterface.BUTTON_POSITIVE);

				buttonsMsg = new ButtonMsg[] { btOK };
			}

			if (!isCustom) {
				for (ButtonMsg botao : buttonsMsg) {
					switch (botao.getTipo()) {
					case DialogInterface.BUTTON_POSITIVE:
						retorno.setPositiveButton(botao.getTexto(), botao.getEvento());
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						retorno.setNegativeButton(botao.getTexto(), botao.getEvento());
						break;
					case DialogInterface.BUTTON_NEUTRAL:
						retorno.setNeutralButton(botao.getTexto(), botao.getEvento());
						break;
					}
				}
			} else {
				LinearLayout llGeral = new LinearLayout(context);
				LinearLayout llConteudo = new LinearLayout(context);

				TableLayout tlBotoes = new TableLayout(context);
				TableRow tr = new TableRow(context);
				tlBotoes.addView(tr);

				tlBotoes.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
				tr.setLayoutParams(new TableRow.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

				llConteudo.addView(conteudo);

				llGeral.setOrientation(LinearLayout.VERTICAL);
				llGeral.addView(llConteudo);
				llGeral.addView(tlBotoes);

				for (ButtonMsg botao : buttonsMsg) {
					Button bt = new Button(context);
					bt.setText(botao.getTexto());
					bt.setTag(botao);

					bt.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View arg0) {
							if (arg0.getTag() != null) {
								if (((ButtonMsg) arg0.getTag()).getEvento() != null)
									((ButtonMsg) arg0.getTag()).getEvento().onClick(retorno.create(), ((ButtonMsg) arg0.getTag()).getTipo());
								else
									retorno.create().dismiss();
							}
						}
					});
					tr.addView(bt);
				}

				retorno.setView(llGeral);
			}

			if (showKey) {
				InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(0, 0);
			}

			return retorno;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Retorna um Dialog que pode ser exibido ou não
	 * 
	 * @param titulo
	 * @param mensagem
	 * @param botoes
	 * @return
	 */

	public static Dialog showMessage(String titulo, String mensagem, ButtonMsg[] botoes, Context context) {
		return showMessage(titulo, mensagem, botoes, 0, context);
	}

	public static Dialog showMessage(String titulo, String mensagem, DialogInterface.OnClickListener eventoOK, String tituloBotaoOK,
			String tituloBotaoCancel, Context context) {
		ButtonMsg btOK = new ButtonMsg(tituloBotaoOK, eventoOK);
		btOK.setTipo(DialogInterface.BUTTON_POSITIVE);
		
		ButtonMsg btCancel;
		ButtonMsg[] listaBotoes;
		if (tituloBotaoCancel != null) {
			btCancel = new ButtonMsg(tituloBotaoCancel, null);
			btCancel.setTipo(DialogInterface.BUTTON_NEGATIVE);
			listaBotoes = new ButtonMsg[] { btOK, btCancel };
		} else
			listaBotoes = new ButtonMsg[] { btOK };
		
		return showMessage(titulo, mensagem, listaBotoes, 0, context);
	}
	
	public static Dialog showMessage(String titulo, String mensagem, ButtonMsg[] botoes, int leftIcon, Context context) {
		AlertDialog novaMensagem = criarDialogo(context, titulo, mensagem, botoes, null, false, false, leftIcon).create();

		openedMessages.add(novaMensagem);
		return novaMensagem;
	}

	/**
	 * Retorna um Dialog com conteúdo personalizado, que pode ser exibido ou não
	 * 
	 * @param titulo
	 * @param mensagem
	 * @param botoes
	 * @param conteudo
	 * @return
	 */

	public static Dialog showMessage(String titulo, String mensagem, ButtonMsg[] botoes, View conteudo, Boolean showKey, Context context) {
		AlertDialog novaMensagem = criarDialogo(context, titulo, mensagem, botoes, conteudo, false, showKey, 0).create();
		openedMessages.add(novaMensagem);
		return novaMensagem;
	}

	public static Dialog showMessage(String titulo, String mensagem, ButtonMsg[] botoes, View conteudo, Context context) {
		AlertDialog novaMensagem = criarDialogo(context, titulo, mensagem, botoes, conteudo, false, false, 0).create();
		openedMessages.add(novaMensagem);
		return novaMensagem;
	}

	public static Dialog inputBox(String titulo, String mensagem, EditText compEntrada, ButtonMsg botaoOK, Context context) {
		ButtonMsg botaoCancel = new ButtonMsg("Cancelar", null);
		botaoCancel.setTipo(DialogInterface.BUTTON_NEGATIVE);
		botaoOK.setTipo(DialogInterface.BUTTON_POSITIVE);

		AlertDialog novaMensagem = criarDialogo(context, titulo, mensagem, new ButtonMsg[] { botaoOK, botaoCancel }, compEntrada, false, false, 0).create();
		openedMessages.add(novaMensagem);
		return novaMensagem;
	}

	/**
	 * Retorna um Dialog com conteúdo personalizado, que pode ser exibido ou
	 * não, sem exibir os botões padrão do dialog
	 * 
	 * @param titulo
	 * @param mensagem
	 * @param botoes
	 * @param conteudo
	 * @return
	 */
	public static Dialog showMessageNoShowButtons(String titulo, String mensagem, ButtonMsg[] botoes, View conteudo, Context context) {
		AlertDialog novaMensagem = criarDialogo(context, titulo, mensagem, botoes, conteudo, true, false, 0).create();
		openedMessages.add(novaMensagem);
		return novaMensagem;
	}

	/**
	 * Exibe um Toast no device
	 * 
	 * @param texto
	 */
	public static void showNotification(String texto, Context context) {
		CharSequence text = texto;
		Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
		toast.show();
	}

	/**
	 * Cria uma caixa de mensagem com o layout informado
	 * 
	 * @param titulo
	 * @param mensagem
	 * @param Positive
	 *            - se for null, não mostra o botão
	 * @param Negative
	 *            - se for null, não mostra o botão
	 * @param layout
	 */

	public static void DialogBox(String titulo, String mensagem, OnClickListener Positive, OnClickListener Negative, View layout, Context context) {
		DialogBox(titulo, mensagem, Positive, Negative, layout, false, context).show();
	}

	public static Dialog DialogBox(String titulo, String mensagem, OnClickListener Positive, OnClickListener Negative, View layout, boolean noButtons, Context context) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle(titulo);
		alert.setCancelable(false);

		includeKeyListener(alert);

		if (mensagem != null && !mensagem.equals(""))
			alert.setMessage(mensagem);
		alert.setView(layout);

		if (Positive != null) {
			alert.setPositiveButton("OK", Positive);
		}

		if (Negative != null) {
			alert.setNegativeButton("Cancelar", Negative);
		}

		/**
		 * Se os dois botões forem passados como null, cria um botão neutro de
		 * OK
		 */
		if (Positive == null && Negative == null && !noButtons) {
			alert.setPositiveButton("OK", null);
		}

		return alert.create();
	}

	/**
	 * Cria e exibe uma caixa de mensagem com um título, uma lista de opções
	 * para o usuário escolher e botão para cancelar a necessidade de seleção e
	 * voltar ao contexto de chamada
	 * 
	 * @param titulo
	 *            Título da mensagem a ser exibida
	 * @param listItens
	 *            Lista de opções para seleção do usuário
	 * @param selectAction
	 *            Ação a ser realizada pós seleção
	 */
	public static void showOptionList(String titulo, CharSequence[] listItens, OnClickListener selectAction, Context context) {
		showOptionList(titulo, listItens, selectAction, null, context);
	}

	public static void showOptionList(String titulo, CharSequence[] listItens, OnClickListener selectAction, OnClickListener cancelListener, Context context) {
		AlertDialog.Builder optionList = new AlertDialog.Builder(context);
		optionList.setTitle(titulo);
		optionList.setItems(listItens, selectAction);

		includeKeyListener(optionList);
		optionList.setNegativeButton("Cancelar", cancelListener);
		optionList.show();
	}

	/**
	 * Impede que a caixa de diálogo seja fechada quando o usuário seleciona uma
	 * das teclas citadas no "IF"
	 * 
	 * @param dialog
	 */
	private static void includeKeyListener(AlertDialog.Builder dialog) {
		dialog.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent keyEvent) {
				if (keyCode == KeyEvent.KEYCODE_SEARCH || keyCode == KeyEvent.KEYCODE_MENU)
					return true;

				return false;
			}
		});
	}

}
