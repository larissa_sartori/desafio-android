package br.com.nrtech.utils;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

public class General {
	public static Locale localBR = new Locale("pt", "BR");

	public static boolean cameraPresente(Context context) {
		PackageManager packageManager = context.getPackageManager();
		return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	public static String dateTimeToXMLString(java.util.Date data) {
		if (data == null)
			return "";

		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(data);
	}

	public static byte[] bmpToArray(Bitmap bmp) {
		if (bmp == null)
			return null;

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();

		return byteArray;
	}

	public static String msToHourSecond(int ms) {

		long segundos = ms / 1000;
		long minutos = segundos / 60;
		segundos = segundos % 60;
		long horas = minutos / 60;
		minutos = minutos % 60;
		String tempo = String.format("%02d:%02d:%02d", horas, minutos, segundos);
		return tempo;
		//
		//
		// if (formato == null)
		// formato = "HH:mm";
		//
		// Calendar c = Calendar.getInstance();
		// c.set(Calendar.HOUR, 0);
		// c.set(Calendar.MINUTE, 0);
		// c.set(Calendar.SECOND, 0);
		// c.set(Calendar.MILLISECOND, 0);
		// c.add(Calendar.MILLISECOND, ms);
		// return new SimpleDateFormat(formato).format(c.getTime());
	}

	public static boolean stringNullOrEmpty(String string) {
		return string == null || string.length() == 0;
	}

	public static Date stringToXMLDate(String texto) {
		if (texto == null)
			return null;

		return stringToDate(texto, "yyyy-MM-dd'T'HH:mm:ss");
	}

	public static Date stringToDate(String texto, String formato) {
		return stringToDate(texto, formato, false);
	}

	public static Date stringToDate(String texto, String formato, boolean nullIfError) {
		try {
			SimpleDateFormat format = new SimpleDateFormat(formato);
			java.sql.Date data = new java.sql.Date(format.parse(texto).getTime());
			return data;
		} catch (Exception ex) {
			ex.printStackTrace();
			return (nullIfError ? null : new Date());
		}
	}

	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	public static void exibirTeclado(Context context) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	public static void ocultarTeclado(Context context) {
		if ((context instanceof Activity) && ((Activity) context).getCurrentFocus() != null) {
			InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
		}
	}

	public static void loadImageFromURL(String url, ImageView imageView, Drawable draw) {
		DrawableBackgroundDownloader drawableDownloader = new DrawableBackgroundDownloader();
		drawableDownloader.loadDrawable(url, imageView, draw);
	}

	public static void loadImageViewFromArray(final Context context, final ImageView imageView, final byte[] byteArray) {
		if (imageView == null || byteArray == null)
			return;

		new Thread(new Runnable() {
			public void run() {
				final Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
				((Activity) context).runOnUiThread(new Runnable() {
					public void run() {
						imageView.setImageBitmap(bmp);
					}
				});
			}
		}).start();
	}

	public static String getAPPVersionName(Context context) {
		PackageInfo pInfo;
		try {
			pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return pInfo.versionName;
		} catch (NameNotFoundException e) {
			ErrorUtils.trataErros(e, null, false, context);
		}

		return "1.0";
	}

}
