package br.com.nrtech.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.Color;

public class ColorUtils {
	public static String color2Hex(int color) {
		return String.format("#%06X", 0xFFFFFF & color);
	}

	public static String getNomeCorByIdx(int idx) {
		return listaNomeCores().get(idx);
	}

	public static List<Integer> getListaCores() {
		ArrayList<Integer> retorno = new ArrayList<Integer>();
		retorno.add(Color.BLACK);
		retorno.add(Color.BLUE);
		retorno.add(Color.CYAN);
		retorno.add(Color.DKGRAY);
		retorno.add(Color.GRAY);
		retorno.add(Color.GREEN);
		retorno.add(Color.MAGENTA);
		retorno.add(Color.RED);
		retorno.add(Color.WHITE);
		retorno.add(Color.YELLOW);

		return retorno;
	}

	public static List<String> listaNomeCores() {
		ArrayList<String> retorno = new ArrayList<String>();
		retorno.add("PRETO");
		retorno.add("AZUL");
		retorno.add("CIANO");
		retorno.add("CINZA-ESCURO");
		retorno.add("CINZA");
		retorno.add("VERDE");
		retorno.add("MAGENTA");
		retorno.add("VERMELHO");
		retorno.add("BRANCO");
		retorno.add("AMARELO");

		return retorno;
	}

	public static int getDominantColor1(Bitmap bitmap, boolean returnIdx) {

		if (bitmap == null)
			throw new NullPointerException();

		Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 25, 25, true);

		int width = scaled.getWidth();
		int height = scaled.getHeight();
		int size = width * height;
		int pixels[] = new int[size];

		Bitmap bitmap2 = scaled.copy(Bitmap.Config.ARGB_4444, false);

		bitmap2.getPixels(pixels, 0, width, 0, 0, width, height);

		final List<HashMap<Integer, Integer>> colorMap = new ArrayList<HashMap<Integer, Integer>>();
		colorMap.add(new HashMap<Integer, Integer>());
		colorMap.add(new HashMap<Integer, Integer>());
		colorMap.add(new HashMap<Integer, Integer>());

		int color = 0;
		int r = 0;
		int g = 0;
		int b = 0;
		Integer rC, gC, bC;
		for (int i = 0; i < pixels.length; i++) {
			color = pixels[i];

			r = Color.red(color);
			g = Color.green(color);
			b = Color.blue(color);

			rC = colorMap.get(0).get(r);
			if (rC == null)
				rC = 0;
			colorMap.get(0).put(r, ++rC);

			gC = colorMap.get(1).get(g);
			if (gC == null)
				gC = 0;
			colorMap.get(1).put(g, ++gC);

			bC = colorMap.get(2).get(b);
			if (bC == null)
				bC = 0;
			colorMap.get(2).put(b, ++bC);
		}

		int[] rgb = new int[3];
		for (int i = 0; i < 3; i++) {
			int max = 0;
			int val = 0;
			for (Map.Entry<Integer, Integer> entry : colorMap.get(i).entrySet()) {
				if (entry.getValue() > max) {
					max = entry.getValue();
					val = entry.getKey();
				}
			}
			rgb[i] = val;
		}

		int dominantColor = Color.rgb(rgb[0], rgb[1], rgb[2]);
		int distancia = 300000;
		int indice = 0;
		for (int cor : getListaCores()) {
			int nova = Distance(dominantColor, cor);
			if (distancia > nova) {
				distancia = nova;
				indice = getListaCores().indexOf(cor);
			}
		}

		// System.out.println("COR: " + getNomeCorByIdx(indice) +
		// " | DISTÂNCIA: " + distancia);
		return returnIdx ? indice : dominantColor;
	}

	public static int Distance(int a, int b) {
		return Math.abs(Color.red(a) - Color.red(b)) + Math.abs(Color.green(a) - Color.green(b)) + Math.abs(Color.blue(a) - Color.blue(b));
	}
}
