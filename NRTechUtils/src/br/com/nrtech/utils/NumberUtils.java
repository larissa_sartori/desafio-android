package br.com.nrtech.utils;

public class NumberUtils {
	public static String formatFormat(Float valor) {
		if (valor == null)
			return "0,00";

		try {
			return String.format(General.localBR, "%.2f", valor);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "0,00";
	}

	public static String formatInteger(Float valor) {
		if (valor == null)
			return "0";

		try {
			return String.format(General.localBR, "%.0f", valor);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "0";
	}

	public static Float tryParseFloat(String original) {
		Float retorno = 0f;
		try {
			retorno = Float.parseFloat(original.replaceAll(",", "\\."));
		} catch (Exception ex) {
			return 0f;
		}

		return retorno;
	}

}
