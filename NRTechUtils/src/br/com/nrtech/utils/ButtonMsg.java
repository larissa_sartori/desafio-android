/**
 * 
 */
package br.com.nrtech.utils;

import android.content.DialogInterface;


public class ButtonMsg {
	private String texto = "";
	private DialogInterface.OnClickListener evento = null;
	private int tipo;

	public ButtonMsg() {
		this.tipo = DialogInterface.BUTTON_NEUTRAL;
	}

	public ButtonMsg(String textoBotao, DialogInterface.OnClickListener eventoBotao) {
		this.texto = textoBotao;
		this.evento = eventoBotao;
		this.tipo = DialogInterface.BUTTON_NEUTRAL;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public DialogInterface.OnClickListener getEvento() {
		return evento;
	}

	public void setEvento(DialogInterface.OnClickListener evento) {
		this.evento = evento;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
}
