package br.com.nrtech.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

public class DeviceUtils {
	public static void chamarTelefone(Context context, String telefone) {
		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telefone));
		context.startActivity(intent);
	}

	public static void enviarEmail(Context context, String destino, String assunto, String conteudo) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { destino });
		i.putExtra(Intent.EXTRA_SUBJECT, assunto);
		i.putExtra(Intent.EXTRA_TEXT, conteudo);
		try {
			context.startActivity(Intent.createChooser(i, "Enviar e-mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(context, "Você não possui nenhum cliente de e-mail instalado.", Toast.LENGTH_SHORT).show();
		}
	}

	public static void abrirSite(Context context, String url) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		context.startActivity(browserIntent);
	}
}
