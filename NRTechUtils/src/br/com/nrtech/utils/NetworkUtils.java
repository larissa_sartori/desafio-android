package br.com.nrtech.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {
	public static boolean isConnected(Context context, boolean showMessageIfNot) {
		try {
			ConnectivityManager cm = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo();

			if (netInfo != null && netInfo.isConnected()) {
				return true;
			} else {
				if (showMessageIfNot)
					Mensagens.showMessage("Sem rede", "Verifique sua conexão",
							null, context).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
