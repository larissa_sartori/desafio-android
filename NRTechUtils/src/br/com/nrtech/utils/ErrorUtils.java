package br.com.nrtech.utils;

import android.content.Context;

public class ErrorUtils {
	public static void trataErros(Exception ex, String msgExibir, boolean show,
			Context context) {
		ex.printStackTrace();
		if (!show || context == null)
			return;

		if (msgExibir == null)
			msgExibir = ex.getMessage();

		Mensagens.showMessage("Erro", msgExibir, null, context).show();
	}

}
